import os
import sys
import math
import curses
from hsaudiotag import mpeg as mp3Tag
from shutil import copy2, disk_usage, get_terminal_size
from hdr_utils import dateof, sizeof, has_id3, is_ignored, strip_extension, \
        format_size, import_from_json
from configuration import DEFAULT_IMPORT


# --- BEGIN UTILITY FUNCTIONS --- #

def _find_total(analysis, extensions=None):
    """
    Find combined total size of all files with the specified extensions.

    Args:
        extensions (dict): aggregate metadata for each extension type
    Returns:
        total (int): total combined size of all files of specified
                     extensions
    """
    total = 0
    if extensions:
        for extension in extensions:
            total += analysis['extensions'][extension]['size']
    else:
        for extension in analysis['extensions']:
            total += analysis['extensions'][extension]['size']

    return total


def _rename_file(filename, date, index):
    """
    Rename ambiguous filenames with dates and incremented index to
    create unique, timestamped filenames.

    If trim is True, extract the original filename if possible and
    use instead of creating a date-based filename.

    Expectes filenames of format:
        [prefix]_[original_file_name].[extension]

    Args:
        filename (str): filename to reformat
        date (str): formatted date to include in new filename
        trim (bool): whether to extract original filename, if any
    Returns:
        new_filename (str): new filename - either date-based or original
    """
    extension = strip_extension(filename)

    # Trim prefix and extension
    filename = filename.partition('.')[0]           # strip extension
    original_name = filename.partition('_')[2]      # strip prefix

    # Use index in place of original name - danger: collisions possible!
    if original_name:
        index = ''

    # Construct new filename
    suffix = str(index) + '.' + extension
    new_filename = date + '_' + original_name + suffix

    return new_filename


def _rename_mp3(filepath):
    """
    Rename mp3 files using intact id3 metadata, namely artist
    and title. Removes any slashes to prevent erroneous path creation.

    Args:
        filepath (str): path to mp3 file
    Returns:
        new_filename (str): filename based on artist and track title
    """
    mp3 = mp3Tag.Mpeg(filepath)
    new_filename = mp3.tag.artist + ' - ' + mp3.tag.title + '.mp3'
    new_filename = new_filename.replace('/', '_')
    return new_filename


def _is_copy_safe(analysis, root='.'):
    """
    Check whether current disk has sufficient space to copy source files.

    Args:
        root (str): root of disk on which to copy
    Returns:
        bool: whether there is enough space to copy source files
    """

    free = disk_usage(root).free
    if free > analysis['total_size']:
        return True
    else:
        print('hd_recover: insufficient disk space to copy source files:'
              + '\n' + 'total bytes to copy: ' + str(analysis['total_size'])
              + '\n' + 'total bytes free: ' + str(free)
              )
        return False


def _get_terminal_scale():
    """
    Determines scale for progress bar based on terminal size. Used
    with curses.

    :returns scale float: percent of terminal width for progress bar
    """
    columns = get_terminal_size().columns
    format_length = len('Total progress: [] 00.0%')
    bar_length = columns - format_length
    scale = bar_length / 100
    return scale

# --- END UTILITY FUNCTIONS --- #

# --- BEGIN PRIVATE METHODS --- #


def _make_destinations(root, analysis, dir_size=500, extensions=None):
    """
    Create destination directories for designated extensions and
    maps extension names to matching destination paths.

    :param root str: root directory path in which to create destinations
    :param extensions list: list of extensions to include in sort
    """
    if not extensions:
        extensions = list(analysis['extensions'])
    for extension in extensions:
        if analysis['extensions'][extension]['count'] > 0:
            base_path = root + '/' + extension.lower()

            # Create subdirectories to split large numbers of files
            # as specifed by dir_size in config
            subdirectories = int(math.ceil(
                analysis['extensions'][extension]['count'] / dir_size
            ))

            for index in range(subdirectories):
                path = base_path + '/' + str(index)
                try:
                    os.makedirs(path)
                except OSError:
                    print('hd_recover sort: ABORTING: {0} exists'.format(root))
                    sys.exit()


def _find_sources(root='.'):
    """
    Find all source files by listing all second children of the
    root directory, i.e. the root directory's childrens' children.

    :param str root: root directory path
    :returns sources dict: map of source directory paths to
                           lists of contained file paths (children)
    """
    sources = {}
    contents = os.listdir(root)             # put contents in bag
    for item in contents:                   # check bag for directories
        if os.path.isdir(item):
            source = root + '/' + item      # build source path
            sources[source] = []

            files = os.listdir(source)
            for file_ in files:
                filepath = source + '/' + file_     # build source path
                if os.path.isfile(filepath):        # map file to source
                    sources[source].append(file_)
    return sources


def _update_subdir_status(subdir_status, extension, dir_size):
    """
    Update the subdir_status to keep track of how many more files, if any,
    can be copied into an extension recovery directory.

    :param subdir_status dict: map of extensions to destination directory info
    :param extension str: extension of file being sorted
    :returns subdir_status dict: map of extensions to
                                 destination directory info
    """
    if subdir_status[extension]['remaining'] == 0:
        subdir_status[extension]['current'] += 1
        subdir_status[extension]['remaining'] = dir_size
    else:
        subdir_status[extension]['remaining'] -= 1
    return subdir_status


def _set_destination_path(sourcepath, filename, root, subdir_status):
    """
    Determine file destination path based on filename and path to
    source file.

    :param sourcepath str: path to source file
    :param root str: root destination directory
    :param filename str: name of the sorted file
    :returns path str: full destination path of sorted file
    """
    date = dateof(sourcepath)
    extension = strip_extension(filename)

    # Create filename
    if extension == 'mp3' and has_id3(sourcepath):
        new_filename = _rename_mp3(sourcepath)
    else:
        try:
            new_filename = _rename_file(
                filename, date, subdir_status[extension]['remaining'])
        except KeyError:
            raise

    # Build path
    path = root + '/' + extension + '/' \
        + str(subdir_status[extension]['current']) + '/' + new_filename
    return path


def _print_progress(stdscr, source, destination, progress, total):
    """
    Print current sort status using curses.

    Args:
        stdscr (object): window object representing entire screen
        source (str): path to source file
        destination (str): path to destination file
        progress (int): amount of total source files sorted in bytes
        total (int): total size of source files to be sorted
    Returns:
        none
    """
    percentage = progress / total * 100
    remaining_bytes = total * (100 - percentage) / 100
    scale = _get_terminal_scale()

    # Count progress bar blocks
    completed = int(percentage * scale)
    remaining = int(scale * 100) - completed
    progress = completed * '#' + remaining * '-'

    stdscr.erase()
    stdscr.addstr(0, 0, 'Copying {0} to {1}...'.format(
        source, destination))
    stdscr.addstr(2, 0, 'Total progress: [{1:50}] {0:.1f}%'.format(
        percentage, progress))
    stdscr.addstr(3, 0,
                  'Remaining: {0}'.format(format_size(remaining_bytes))
                  )
    stdscr.refresh()

# --- END PRIVATE METHODS --- #


# --- BEGIN PUBLIC METHOD --- #

def hdr_sort(input_file=None, old_root='.', new_root='hdr_sorted',
             extensions=None, dir_size=None, no_skipped=False):
    """
    Rename files with selected extensions and sort into matching
    extension directories and subdirectories based on configuration
    settings.

    :param input_file str: path to existing analysis file
    :param old_root str: root directory of source files
    :param new_root str: destination root directory to sort files into
    :param extenstions list: list of extensions to include in sort
    :param dir_size int: number of files to store in each recovery directory
    """
    print('hd_recover sort: loading analysis... ', end='')
    if input_file is None:
        analysis = import_from_json(DEFAULT_IMPORT)
    else:
        analysis = import_from_json(input_file)
    print('OK')

    if not _is_copy_safe(analysis, old_root):
        print('hd_recover sort: ABORTING: not enough space for copy')
        return

    if not dir_size:
        dir_size = analysis['config']['dir_size']

    if not extensions:
        extensions = list(analysis['extensions'])

    if not new_root:
        new_root = 'hdr_sort'

    # Initialize subdirectory status dictionary to track number of files
    # in a subdirectory for a given extension.
    subdir_status = {}
    for extension in extensions:
        subdir_status[extension] = {'current': 0, 'remaining': dir_size}

    print('hd_recover sort: creating destination directories... ', end='')
    _make_destinations(
        new_root, analysis, dir_size=dir_size, extensions=extensions)
    print('OK')

    print('hd_recover sort: finding source files... ', end='')
    sources = _find_sources(old_root)
    print('OK')

    # Number of files sorted, ignored, or skipped (due to errors)
    results = {'sorted': 0, 'ignored': 0, 'skipped': 0}
    skipped_files = []

    progress = 0
    total = _find_total(analysis, extensions)
    try:
        stdscr = curses.initscr()       # initialize curses

        for directory in sources:
            for item in sources[directory]:
                sourcepath = directory + '/' + item
                extension = strip_extension(item)
                if is_ignored(sourcepath, analysis['config']):
                    results['ignored'] += 1
                    continue

                try:
                    destination = _set_destination_path(
                        sourcepath, item, new_root, subdir_status)
                except KeyError:
                    results['skipped'] += 1
                    continue

                try:
                    copy2(sourcepath, destination)
                    subdir_status = _update_subdir_status(
                        subdir_status, extension, dir_size)
                except FileNotFoundError:
                    skipped_files.append(sourcepath + ' to ' + destination)
                    results['skipped'] += 1
                    continue

                results['sorted'] += 1
                progress += sizeof(sourcepath)

                # create progress bar with curses
                _print_progress(
                    stdscr, sourcepath, destination, progress, total)
        curses.endwin()     # clean up after curses
    except KeyboardInterrupt:
        curses.endwin()

    print('Files sorted: ' + str(results['sorted'])
          + '\nFiles ignored: ' + str(results['ignored'])
          + '\nFiles skipped: ' + str(results['skipped']))

    if not no_skipped:
        try:
            print('hd_recover sort: exporting skipped file list to '
                  + 'hdr_skipped.txt... ', end='')
            with open('hdr_skipped', 'w') as file_:
                for item in skipped_files:
                    file_.write(item + '\n')
            print('OK')
        except NotADirectoryError:
            # Output to stdout in case user needs skipped details,
            # allowing review without need to re-run the program
            print('FAILED.')
            print('Listing skipped files...')
            for item in skipped_files:
                print(item)
            return

# --- END PUBLIC METHOD --- #
