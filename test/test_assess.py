import unittest
from unittest import mock
from unittest.mock import call
from hd_recover import assess
from hd_recover.hdr_utils import format_size


class AssessTestCase(unittest.TestCase):
    def test__print_item(self):
        # Check print called with proper data when count > 0
        with mock.patch('hd_recover.assess.print') as fake_print:
            dates = {'1975-06-10': {'count': 1, 'size': 200, 'percent': 100.0}}
            assess._print_item(dates, '1975-06-10', 1)
            fake_print.assert_called_once_with(
                '{:5}'.format(str(1) + '. ')
                + '{:12}'.format('1975-06-10' + '')
                + '{:>10}'.format(str(1))
                + '{:>10}'.format(format_size(200))
                + '{:>10.2f}'.format(100.0) + '%'
            )

        # Check proper count returned when:
        #   data item count > 0
        with mock.patch('hd_recover.assess.print') as fake_print:
            dates = {'1975-06-10': {'count': 1, 'size': 200, 'percent': 100.0}}
            count = assess._print_item(dates, '1975-06-10', 1)
            self.assertEqual(2, count)

        #   data item count <= 0
        with mock.patch('hd_recover.assess.print') as fake_print:
            dates = {'1975-06-10': {'count': 0, 'size': 200, 'percent': 100.0}}
            count = assess._print_item(dates, '1975-06-10', 1)
            self.assertEqual(1, count)

    # --- END TEST PRIVATE METHODS --- #

    # --- BEGIN PUBLIC METHOD --- #

    def test_hdr_assess(self):
        analysis = {
            'extensions': {
                'jpg': {'count': 2, 'size': 1000, 'percent': 50.0},
            },
            'dates': {
                '1975-06-10': {'count': 2, 'size': 1000, 'percent': 50.0},
            },
            'ignored': {'count': 0, 'size': 0, 'percent': 0.0},
            'total_size': 2000
        }
        # Check arg 'field' set to 'count' when arg not in tuple
        with mock.patch('hd_recover.assess.import_from_json') as fake_json:
            with mock.patch('hd_recover.assess._print_item') as fake_print_i:
                with mock.patch('hd_recover.assess.print') as fake_print:
                    fake_json.return_value = analysis
                    assess.hdr_assess(
                        input_file='some file',
                        n=2,
                        by_date=False,
                        field='test',
                        asc=False
                    )

                    fake_print_i.assert_called_with(
                            analysis['extensions'], 'jpg', 1)

        # Check arg 'n' properly validated:
        #   convert to int
        with mock.patch('hd_recover.assess.import_from_json') as fake_json:
            with mock.patch('hd_recover.assess._print_item') as fake_print_i:
                with mock.patch('hd_recover.assess.print') as fake_print:
                    fake_json.return_value = analysis
                    assess.hdr_assess(
                        input_file='some file',
                        n='1',
                        by_date=False,
                        field='count',
                        asc=False
                    )

                    fake_print_i.assert_called_with(
                            analysis['extensions'], 'jpg', 1)

        #   n cannot be converted to int
        with mock.patch('hd_recover.assess.import_from_json') as fake_json:
            with mock.patch('hd_recover.assess._print_item') as fake_print_i:
                with mock.patch('hd_recover.assess.print') as fake_print:
                    fake_json.return_value = analysis
                    assess.hdr_assess(
                        input_file='some file',
                        n='test',
                        by_date=False,
                        field='count',
                        asc=False
                    )

                    fake_print_i.assert_called_with(
                            analysis['extensions'], 'jpg', 1)

        #   n <= 0
        with mock.patch('hd_recover.assess.import_from_json') as fake_json:
            with mock.patch('hd_recover.assess._print_item') as fake_print_i:
                with mock.patch('hd_recover.assess.print') as fake_print:
                    fake_json.return_value = analysis
                    assess.hdr_assess(
                        input_file='some file',
                        n=0,
                        by_date=False,
                        field='test',
                        asc=False
                    )

                    fake_print_i.assert_called_with(
                            analysis['extensions'], 'jpg', 1)

        # Check error returned for mutually exclusive options (n and ext)
        with mock.patch('hd_recover.assess.import_from_json') as fake_json:
            with mock.patch('hd_recover.assess._print_item') as fake_print_i:
                with mock.patch('hd_recover.assess.print') as fake_print:
                    fake_json.return_value = analysis
                    assess.hdr_assess(
                        input_file='some file',
                        n=1,
                        extensions=['mp3', 'jpg'],
                        by_date=False,
                        field='test',
                        asc=False
                    )

                    fake_print.assert_called_once_with(
                        'hd_recover assess: ABORTING: cannot pass number and '
                        + 'extension')

        # Check sorted called when by_date set:
        with mock.patch('hd_recover.assess.import_from_json') as fake_json:
            with mock.patch('hd_recover.assess._print_item') as fake_print_i:
                with mock.patch('hd_recover.assess.print') as fake_print:
                    with mock.patch('hd_recover.assess.sorted') as fake_sorted:
                        fake_json.return_value = analysis
                        assess.hdr_assess(
                            input_file='some file',
                            n=1,
                            by_date=False,
                            field='test',
                            asc=False
                        )

                        fake_sorted.assert_called_once()

        # Check sorted called when by_date not set:
        with mock.patch('hd_recover.assess.import_from_json') as fake_json:
            with mock.patch('hd_recover.assess._print_item') as fake_print_i:
                with mock.patch('hd_recover.assess.print') as fake_print:
                    with mock.patch('hd_recover.assess.sorted') as fake_sorted:
                        fake_json.return_value = analysis
                        assess.hdr_assess(
                            input_file='some file',
                            n=1,
                            by_date=False,
                            field='test',
                            asc=False
                        )

                        fake_sorted.assert_called_once()

        # Check items sorted with extensions from CLI:
        with mock.patch('hd_recover.assess.import_from_json') as fake_json:
            with mock.patch('hd_recover.assess._print_item') as fake_print_i:
                with mock.patch('hd_recover.assess.print') as fake_print:
                    with mock.patch('hd_recover.assess.sorted') as fake_sorted:
                        fake_json.return_value = analysis
                        assess.hdr_assess(
                            input_file='some file',
                            extensions=['mp3', 'jpg'],
                            by_date=False,
                            field='test',
                            asc=False
                        )

                        extensions = ['mp3', 'jpg']
                        fake_sorted.assert_called_once_with(extensions,
                                                            reverse=True)

        # Check print called with correct ignored_files details
        with mock.patch('hd_recover.assess.import_from_json') as fake_json:
            with mock.patch('hd_recover.assess._print_item') as fake_print_i:
                with mock.patch('hd_recover.assess.print') as fake_print:
                    fake_json.return_value = analysis
                    assess.hdr_assess(
                        input_file='some file',
                        n=1,
                        by_date=False,
                        field='test',
                        asc=False
                    )

                    fake_print.assert_has_calls([
                        call(
                            '\nIgnored files : '
                            + '{:>10}'.format(str(0))
                            + '{:>10}'.format(format_size(str(0)))
                            + '{:>12.2f}'.format(0.0)
                            + '%\n'),
                        call('')
                    ])

    # --- END TEST PUBLIC METHOD --- #


if __name__ == '__main__':
    unittest.main()
