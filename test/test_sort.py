import os
import unittest
from unittest import mock
from unittest.mock import call
from hd_recover import sort
from hd_recover.configuration import DEFAULT_IMPORT, DEFAULT_CONFIG


class SortTestCase(unittest.TestCase):

    # ----- BEGIN TEST PRIVATE METHODS ----- #
    def setUp(self):
        self.analysis = {
            'extensions': {
                'jpg': {'count': 1, 'size': 1000, 'percent': 33.3},
                'mp3': {'count': 1, 'size': 1000, 'percent': 33.3},
            },
            'dates': {
                '1975-06-10': {'count': 2, 'size': 2000, 'percent': 66.6},
            },
            'ignored': {'count': 1, 'size': 1000, 'percent': 33.3},
            'total_size': 3000,
            'config': DEFAULT_CONFIG
        }

    def test__find_total(self):
        # Check for correct return with no extensions passed
        self.assertEqual(sort._find_total(self.analysis), 2000)

        # Check for correct return with extensions passed
        self.assertEqual(sort._find_total(self.analysis, ['mp3']), 1000)

    def test__rename_file(self):
        filename = 'tester.jpg'
        date = '1975-06-10'
        # Check for proper return with simple filename, i.e file.extension
        self.assertEqual(sort._rename_file(filename, date, 1),
                         '1975-06-10_1.jpg')

        # Check for proper return with original name
        filename = 'prefix_tester.jpg'
        self.assertEqual(sort._rename_file(filename, date, 1),
                         '1975-06-10_tester.jpg')

    def test__rename_mp3(self):
        with mock.patch('hd_recover.sort.mp3Tag') as fake_mp3Tag:
            # Check proper return with well-formed id3 tag
            fake_mp3Tag.Mpeg.return_value = mock.Mock()
            fake_mp3Tag.Mpeg.return_value.tag = mock.Mock()
            fake_mp3Tag.Mpeg.return_value.tag.artist = 'Prince'
            fake_mp3Tag.Mpeg.return_value.tag.title = '1999'
            self.assertEqual(sort._rename_mp3('some file'),
                             'Prince - 1999.mp3')

            # Check proper return with malformed id3 tag
            # i.e. path-mangling directory slashes
            fake_mp3Tag.Mpeg.return_value.tag.artist = 'Queen / David Bowie'
            fake_mp3Tag.Mpeg.return_value.tag.title = 'Under Pressure'
            self.assertEqual(sort._rename_mp3('some file'),
                             'Queen _ David Bowie - Under Pressure.mp3')

    def test__is_copy_safe(self):
        with mock.patch('hd_recover.sort.disk_usage') as fake_usage:
            # Check for correct return when sufficient free space
            fake_usage.return_value = mock.Mock()
            fake_usage.return_value.free = 10000
            self.assertTrue(sort._is_copy_safe(self.analysis, 'some path'))

            # Check for correct return when insufficient free space
            fake_usage.return_value.free = 0
            self.assertFalse(sort._is_copy_safe(self.analysis, 'some path'))

    def test__get_terminal_scale(self):
        with mock.patch('hd_recover.sort.get_terminal_size') as fake_getterm:
            fake_getterm.return_value = mock.Mock()
            fake_getterm.return_value.columns = 100
            result = sort._get_terminal_scale()

            # Check get_terminal size called, correct return
            fake_getterm.assert_called_once_with()
            self.assertTrue(isinstance(result, float))
            self.assertTrue(result <= 1 and result > 0)

    def test__make_destinations(self):
        self.analysis['extensions']['jpg']['count'] = 1001
        self.analysis['extensions']['mp3']['count'] = 1000

        # Check os.makedirs called on valid root with no specified extensions
        with mock.patch('hd_recover.sort.os') as fake_os:
            sort._make_destinations('hdr_sorted', self.analysis, 500)
            fake_os.makedirs.assert_has_calls([
                call('hdr_sorted/jpg/0'),
                call('hdr_sorted/jpg/1'),
                call('hdr_sorted/jpg/2'),
                call('hdr_sorted/mp3/0'),
                call('hdr_sorted/mp3/1')
            ])

        # Check os.makedirs called on valid root with specified extensions
        with mock.patch('hd_recover.sort.os') as fake_os:
            sort._make_destinations('hdr_sorted', self.analysis, 500, ['mp3'])
            fake_os.makedirs.assert_has_calls([
                call('hdr_sorted/mp3/0'),
                call('hdr_sorted/mp3/1')
            ])

        # Check prints error and returns on existing file/directory
        with mock.patch('hd_recover.sort.os') as fake_os:
            with mock.patch('hd_recover.sort.sys') as fake_sys:
                fake_os.makedirs.side_effect = OSError
                fake_sys.exit.side_effect = SystemExit
                try:
                    sort._make_destinations(
                        'hdr_sorted', self.analysis, 500, ['mp3'])
                except SystemExit:
                    assert True
                else:
                    assert False
                fake_sys.exit.assert_called_once_with()

    def test__find_sources(self):
        # Simulate os.isdir functionality
        def isdir_side_effect(*args, **kwargs):
            for arg in args:
                if 'dir' in arg:
                    return True
                else:
                    return False

        # Simulate os.isfile functionality
        def isfile_side_effect(*args, **kwargs):
            for arg in args:
                if 'file' in arg:
                    return True
                else:
                    return False

        # Check correct return, i.e. directories ignored, files included
        dummy_contents = ['dir1', 'dir2', 'dir3', 'file1', 'file2', 'file3']
        with mock.patch('hd_recover.sort.os') as fake_os:
            fake_os.listdir.return_value = dummy_contents
            fake_os.path.isdir.side_effect = isdir_side_effect
            fake_os.path.isfile.side_effect = isfile_side_effect
            result = sort._find_sources('some root')
            expected = {
                'some root/dir1': ['file1', 'file2', 'file3'],
                'some root/dir2': ['file1', 'file2', 'file3'],
                'some root/dir3': ['file1', 'file2', 'file3']
            }
            self.assertDictEqual(result, expected)

    def test__update_subdir_status(self):
        subdir_status = {
            'jpg': {'current': 0, 'remaining': 1},
            'mp3': {'current': 0, 'remaining': 0}
        }
        # Check status increment/reset when no more space in subdirectory
        result = sort._update_subdir_status(subdir_status, 'mp3', 500)
        subdir_status['mp3'] = {'current': 1, 'remaining': 500}
        self.assertDictEqual(subdir_status, result)

        # Check decrement when space left in subdirectory
        result = sort._update_subdir_status(subdir_status, 'jpg', 500)
        subdir_status['jpg'] = {'current': 0, 'remaining': 0}
        self.assertDictEqual(subdir_status, result)

    def test__set_destination_path(self):
        subdir_status = {
            'jpg': {'current': 0, 'remaining': 1},
            'mp3': {'current': 0, 'remaining': 500}
        }
        with mock.patch('hd_recover.sort.dateof') as fake_dateof:
            with mock.patch('hd_recover.sort.has_id3') as fake_id3:
                with mock.patch('hd_recover.sort._rename_mp3') as fake_mp3:
                    fake_dateof.return_value = '1975-06-10'

                    # Check correct return for mp3
                    fake_id3.return_value = True
                    fake_mp3.return_value = 'test.mp3'
                    result = sort._set_destination_path(
                        'dir1/test.mp3', 'test.mp3', 'hdr_sorted',
                        subdir_status)
                    self.assertEqual(result, 'hdr_sorted/mp3/0/test.mp3')

                    # Check correct return for mp3 with no id3
                    fake_id3.return_value = False
                    fake_mp3.return_value = 'test.mp3'
                    result = sort._set_destination_path(
                        'dir1/test.mp3', 'test.mp3', 'hdr_sorted',
                        subdir_status)
                    self.assertEqual(
                        result, 'hdr_sorted/mp3/0/1975-06-10_500.mp3')

                    # Check correct return for non-mp3s
                    fake_id3.return_value = False
                    fake_mp3.return_value = 'test.jpg'
                    result = sort._set_destination_path(
                        'dir1/test.jpg', 'test.jpg', 'hdr_sorted',
                        subdir_status)
                    self.assertEqual(
                        result, 'hdr_sorted/jpg/0/1975-06-10_1.jpg')

                    # Check KeyError raise
                    with mock.patch('hd_recover.sort._rename_file') \
                            as fake_rename:
                        fake_rename.side_effect = KeyError
                        try:
                            result = sort._set_destination_path(
                                'dir1/test.jpg', 'test.jpg', 'hdr_sorted',
                                subdir_status)
                        except KeyError:
                            assert True
                        else:
                            assert False

    def test__print_progress(self):
        with mock.patch('hd_recover.sort._get_terminal_scale') as fake_scale:
            with mock.patch('hd_recover.sort.format_size') as fake_format:
                fake_stdscr = mock.Mock()
                fake_scale.return_value = 0.5
                fake_format.return_value = '10.0B'
                sort._print_progress(
                    fake_stdscr, 'some/path', 'other/path', 1000, 2000)

                # Check correct functions called
                fake_scale.assert_called_once_with()
                fake_stdscr.erase.assert_called_once_with()
                fake_stdscr.addstr.assert_called()
                fake_format.assert_called_once()
                fake_stdscr.refresh.assert_called_once_with()

    # ----- BEGIN TEST PRIVATE METHODS ----- #

    # ----- BEGIN TEST PUBLIC METHOD ----- #

    @mock.patch('hd_recover.sort._is_copy_safe')
    def test_hdr_sort(self, fake_safe):
        # Check correct import when input_file not specified
        with mock.patch('hd_recover.sort._make_destinations') as fake_mkdir:
            with mock.patch('hd_recover.sort.import_from_json') as fake_json:
                with mock.patch('hd_recover.sort.curses') as fake_curses:
                    with mock.patch('hd_recover.sort.copy2') as fake_copy:
                        fake_json.return_value = self.analysis
                        sort.hdr_sort()
                        fake_json.assert_called_once_with(DEFAULT_IMPORT)

        # Check correct import when input_file specified
        with mock.patch('hd_recover.sort._make_destinations') as fake_mkdir:
            with mock.patch('hd_recover.sort.import_from_json') as fake_json:
                with mock.patch('hd_recover.sort.curses') as fake_curses:
                    with mock.patch('hd_recover.sort.copy2') as fake_copy:
                        fake_json.return_value = self.analysis
                        sort.hdr_sort(input_file='some_file')
                        fake_json.assert_called_once_with('some_file')

        # Check behavior on failed _is_copy_safe
        with mock.patch('hd_recover.sort._make_destinations') as fake_mkdir:
            with mock.patch('hd_recover.sort.import_from_json') as fake_json:
                with mock.patch('hd_recover.sort.curses') as fake_curses:
                    with mock.patch('hd_recover.sort.copy2') as fake_copy:
                        with mock.patch('hd_recover.sort.print') \
                                as fake_print:
                            fake_json.return_value = self.analysis
                            fake_safe.return_value = False
                            sort.hdr_sort()
                            fake_print.assert_any_call(
                                'hd_recover sort: ABORTING: not enough'
                                + ' space for copy'
                            )

        # Check _make_destinations call with right args when no extensions set
        with mock.patch('hd_recover.sort._make_destinations') as fake_mkdir:
            with mock.patch('hd_recover.sort.import_from_json') as fake_json:
                with mock.patch('hd_recover.sort.curses') as fake_curses:
                    with mock.patch('hd_recover.sort.copy2') as fake_copy:
                        fake_safe.return_value = True
                        fake_json.return_value = self.analysis
                        sort.hdr_sort()
                        fake_mkdir.assert_called_once_with(
                            'hdr_sorted', self.analysis, dir_size=500,
                            extensions=list(self.analysis['extensions']))

        # Check _make_destinations call with right args when extensions set
        with mock.patch('hd_recover.sort._make_destinations') as fake_mkdir:
            with mock.patch('hd_recover.sort.import_from_json') as fake_json:
                with mock.patch('hd_recover.sort.curses') as fake_curses:
                    with mock.patch('hd_recover.sort.copy2') as fake_copy:
                        fake_safe.return_value = True
                        fake_json.return_value = self.analysis
                        sort.hdr_sort(extensions=['mp3'])
                        fake_mkdir.assert_called_once_with(
                            'hdr_sorted', self.analysis, dir_size=500,
                            extensions=['mp3'])

        # Check _find_sources called with correct args
        with mock.patch('hd_recover.sort._make_destinations') as fake_mkdir:
            with mock.patch('hd_recover.sort.import_from_json') as fake_json:
                with mock.patch('hd_recover.sort.curses') as fake_curses:
                    with mock.patch('hd_recover.sort.copy2') as fake_copy:
                        fake_safe.return_value = True
                        fake_json.return_value = self.analysis
                        # default arg
                        with mock.patch('hd_recover.sort._find_sources') \
                                as fake_find:
                            sort.hdr_sort()
                            fake_find.assert_called_once_with('.')

                        with mock.patch('hd_recover.sort._find_sources') \
                                as fake_find:
                            sort.hdr_sort(old_root='some file')
                            fake_find.assert_called_once_with('some file')

        # Check _find_total called with correct args
        with mock.patch('hd_recover.sort._make_destinations') as fake_mkdir:
            with mock.patch('hd_recover.sort.import_from_json') as fake_json:
                with mock.patch('hd_recover.sort.curses') as fake_curses:
                    with mock.patch('hd_recover.sort.copy2') as fake_copy:
                        fake_safe.return_value = True
                        fake_json.return_value = self.analysis
                        with mock.patch('hd_recover.sort._find_total') \
                                as fake_find:
                            sort.hdr_sort()
                            fake_find.assert_called_once_with(
                                self.analysis,
                                list(self.analysis['extensions'])
                            )

        # Check curses.initscr() called
        with mock.patch('hd_recover.sort._make_destinations') as fake_mkdir:
            with mock.patch('hd_recover.sort.import_from_json') as fake_json:
                with mock.patch('hd_recover.sort.curses') as fake_curses:
                    with mock.patch('hd_recover.sort.copy2') as fake_copy:
                        fake_safe.return_value = True
                        fake_json.return_value = self.analysis
                        sort.hdr_sort()
                        fake_curses.initscr.assert_called_once_with()

        # Check curses.endwin() called on successful completion
        with mock.patch('hd_recover.sort._make_destinations') as fake_mkdir:
            with mock.patch('hd_recover.sort.import_from_json') as fake_json:
                with mock.patch('hd_recover.sort.curses') as fake_curses:
                    with mock.patch('hd_recover.sort.copy2') as fake_copy:
                        fake_safe.return_value = True
                        fake_json.return_value = self.analysis
                        sort.hdr_sort()
                        fake_curses.endwin.assert_called_once_with()

        # Check open() called for skipped_files export
        with mock.patch('hd_recover.sort._make_destinations') as fake_mkdir:
            with mock.patch('hd_recover.sort.import_from_json') as fake_json:
                with mock.patch('hd_recover.sort.curses') as fake_curses:
                    with mock.patch('hd_recover.sort.copy2') as fake_copy:
                        fake_safe.return_value = True
                        fake_json.return_value = self.analysis
                        with mock.patch('hd_recover.sort.open') as fake_open:
                            sort.hdr_sort()
                            fake_open.assert_called_once_with(
                                'hdr_skipped', 'w')

        # Check skipped_files not printed if no_skipped is set
        with mock.patch('hd_recover.sort._make_destinations') as fake_mkdir:
            with mock.patch('hd_recover.sort.import_from_json') as fake_json:
                with mock.patch('hd_recover.sort.curses') as fake_curses:
                    with mock.patch('hd_recover.sort.copy2') as fake_copy:
                        fake_safe.return_value = True
                        fake_json.return_value = self.analysis
                        with mock.patch('hd_recover.sort.open') as fake_open:
                            sort.hdr_sort(no_skipped=True)
                            fake_open.assert_not_called()

        # Check skipped_files printed and return on NotADirectoryError
        with mock.patch('hd_recover.sort._make_destinations') as fake_mkdir:
            with mock.patch('hd_recover.sort.import_from_json') as fake_json:
                with mock.patch('hd_recover.sort.curses') as fake_curses:
                    with mock.patch('hd_recover.sort.copy2') as fake_copy:
                        fake_safe.return_value = True
                        fake_json.return_value = self.analysis
                        with mock.patch('hd_recover.sort.open') as fake_open:
                            with mock.patch('hd_recover.sort.print') \
                                    as fake_print:
                                fake_open.side_effect = NotADirectoryError
                                sort.hdr_sort()
                                fake_print.assert_any_call('FAILED.')

        # Clean up - only applies to this test
        os.remove('hdr_skipped')
        os.remove('some file')

    # ----- END TEST PUBLIC METHOD ----- #


if __name__ == '__main__':
    unittest.main()
