import unittest
from unittest import mock
from hd_recover import analyze
from hd_recover.configuration import DEFAULT_CONFIG


class AnalyzeTestCase(unittest.TestCase):

    # --- BEGIN TEST ANALYSIS METHODS --- #

    def test__initialize_extension_map(self):
        # Check included extensions properly initialized
        config = {'extensions': {'mp3': True, 'jpg': True, 'gif': True}}
        extensions = {
            'mp3': {'count': 0, 'size': 0, 'percent': 0},
            'jpg': {'count': 0, 'size': 0, 'percent': 0},
            'gif': {'count': 0, 'size': 0, 'percent': 0}
        }
        initialized_extensions = analyze._initialize_extension_map(config)
        self.assertDictEqual(initialized_extensions, extensions)

    def test__list_directories(self):
        # Check correct directory list returned
        with mock.patch('hd_recover.analyze.os') as fake_os:
            fake_os.listdir.return_value = ['tester', 'test', 'testz']
            fake_os.path.isdir.return_value = True
            directories = analyze._list_directories()
            self.assertListEqual(directories, ['tester', 'test', 'testz'])

    @mock.patch('hd_recover.analyze.os')
    def test__get_file_metadata(self, fake_os):
        directories = ['test1', 'test2']
        test_files = ['fake1.mp3', 'fake2.jpg']
        with mock.patch('hd_recover.analyze.is_ignored') as fake_is_ignored:
            with mock.patch('hd_recover.analyze.sizeof') as fake_sizeof:
                with mock.patch('hd_recover.analyze.dateof') as fake_dateof:
                    # Check analysis['ignored_files'] has correct values
                    analysis = {
                        'extensions': {
                            'mp3': {'count': 0, 'size': 0, 'percent': 0},
                            'jpg': {'count': 0, 'size': 0, 'percent': 0}
                        },
                        'dates': {},
                        'ignored': {'count': 0, 'size': 0, 'percent': 0},
                        'total_size': 0,
                        'config': DEFAULT_CONFIG
                    }
                    fake_is_ignored.return_value = True
                    test_size = DEFAULT_CONFIG['min_size'] - 1000
                    fake_sizeof.return_value = test_size
                    fake_os.listdir.return_value = test_files

                    new_analysis = analyze._get_file_metadata(
                        directories, analysis)
                    self.assertDictEqual(
                        new_analysis['ignored'],
                        {'count': 4, 'size': test_size * 4, 'percent': 0}
                    )

                    # Check analysis['extensions'] has correct values
                    fake_is_ignored.return_value = False
                    test_size = DEFAULT_CONFIG['min_size'] + 1000
                    fake_sizeof.return_value = test_size

                    new_analysis = analyze._get_file_metadata(
                        directories, analysis)
                    self.assertDictEqual(
                        new_analysis['extensions'],
                        {
                            'jpg': {
                                'count': 2, 'size': 2 * test_size, 'percent': 0
                            },
                            'mp3': {
                                'count': 2, 'size': 2 * test_size, 'percent': 0
                            }
                        }
                    )

                    # Check analysis['dates'] has correct values
                    analysis = {
                        'extensions': {
                            'mp3': {'count': 0, 'size': 0, 'percent': 0},
                            'jpg': {'count': 0, 'size': 0, 'percent': 0}
                        },
                        'dates': {},
                        'ignored': {'count': 0, 'size': 0, 'percent': 0},
                        'total_size': 0,
                        'config': DEFAULT_CONFIG
                    }
                    fake_dateof.return_value = '1975-06-10'
                    new_analysis = analyze._get_file_metadata(
                        directories, analysis)
                    self.assertDictEqual(
                        new_analysis['dates'],
                        {
                            '1975-06-10': {
                                'count': 4, 'size': 4 * test_size, 'percent': 0
                            }
                        }
                    )

    def test__calculate_percentages(self):
        # Check for correct percentages when total_size > 0
        analysis = {
            'extensions': {
                'mp3': {'count': 2, 'size': 500, 'percent': 0},
                'jpg': {'count': 2, 'size': 500, 'percent': 0}
            },
            'dates': {
                '1975-06-10': {
                    'count': 2, 'size': 500, 'percent': 0
                },
                '1975-07-11': {
                    'count': 2, 'size': 500, 'percent': 0
                }
            },
            'ignored': {'count': 0, 'size': 0, 'percent': 0},
            'total_size': 1000
        }
        new_analysis = analyze._calculate_percentages(analysis)
        self.assertEqual(50.0, new_analysis['dates']['1975-06-10']['percent'])
        self.assertEqual(50.0, new_analysis['dates']['1975-07-11']['percent'])
        self.assertEqual(50.0, new_analysis['extensions']['mp3']['percent'])
        self.assertEqual(50.0, new_analysis['extensions']['jpg']['percent'])

        # Check percentages set to 0 when total_size <= 0
        analysis['total_size'] = 0
        new_analysis = analyze._calculate_percentages(analysis)
        self.assertEqual(0.0, new_analysis['dates']['1975-06-10']['percent'])
        self.assertEqual(0.0, new_analysis['dates']['1975-07-11']['percent'])
        self.assertEqual(0.0, new_analysis['extensions']['mp3']['percent'])
        self.assertEqual(0.0, new_analysis['extensions']['jpg']['percent'])

    def test__export_to_json(self):
        analysis = {
            'extensions': {
                'mp3': {'count': 2, 'size': 500, 'percent': 0},
                'jpg': {'count': 2, 'size': 500, 'percent': 0}
            },
            'dates': {
                '1975-06-10': {
                    'count': 2, 'size': 500, 'percent': 0
                },
                '1975-07-11': {
                    'count': 2, 'size': 500, 'percent': 0
                }
            },
            'ignored': {'count': 0, 'size': 0, 'percent': 0},
            'total_size': 1000
        }

        with mock.patch('hd_recover.analyze.open') as fake_open:
            with mock.patch('hd_recover.analyze.json') as fake_json:
                fake_json.dump.return_value = True
                analyze._export_to_json(
                    analysis, 'some file', indent=2)

                # Check open called with output filename
                fake_open.assert_called_once_with('some file', 'w')

                # Check json.dump called with export_data and indent
                fake_json.dump.assert_called_once_with(
                    analysis, fake_open.return_value.__enter__.return_value,
                    indent=2)

    # --- END ANALYSIS METHODS --- #

    # --- BEGIN PUBLIC METHOD --- #

    @mock.patch('hd_recover.analyze._calculate_percentages')
    @mock.patch('hd_recover.analyze._get_file_metadata')
    @mock.patch('hd_recover.analyze._list_directories')
    @mock.patch('hd_recover.analyze._initialize_extension_map')
    @mock.patch('hd_recover.analyze.load_config')
    def test_hdr_analyze(self, fake_load_conf, fake_init_ext_map, fake_listdir,
                         fake_get_metadata, fake_calculate):
        analysis = {
            'extensions': {},
            'dates': {},
            'ignored': {'count': 0, 'size': 0, 'percent': 0},
            'total_size': 0
        }

        config = DEFAULT_CONFIG
        config['extensions'] = {'mp3': True, 'jpg': True}
        directories = ['fake1', 'fake2']

        fake_load_conf.return_value = config
        fake_listdir.return_value = directories
        fake_init_ext_map.return_value = {
            'mp3': {'count': 0, 'size': 0, 'percent': 0},
            'jpg': {'count': 0, 'size': 0, 'percent': 0},
        }
        analysis['extensions'] = {
            'mp3': {'count': 0, 'size': 0, 'percent': 0},
            'jpg': {'count': 0, 'size': 0, 'percent': 0},
        }
        analysis['config'] = config
        fake_get_metadata.return_value = analysis
        fake_calculate.return_value = analysis

        analyze.hdr_analyze(
            ignore_blanks=DEFAULT_CONFIG['ignore_blanks'],
            ignore_thumbnails=DEFAULT_CONFIG['ignore_thumbnails'],
            file_size=DEFAULT_CONFIG['min_size'],
            pixels=DEFAULT_CONFIG['img_mins']['pixels'],
            img_size=DEFAULT_CONFIG['img_mins']['size'],
            extensions=['mp3', 'jpg'],
            output_file='some file'
        )

        # Check load_config called with CLI args
        fake_load_conf.assert_called_once_with(
            ignore_blanks=DEFAULT_CONFIG['ignore_blanks'],
            ignore_thumbnails=DEFAULT_CONFIG['ignore_thumbnails'],
            file_size=DEFAULT_CONFIG['min_size'],
            pixels=DEFAULT_CONFIG['img_mins']['pixels'],
            img_size=DEFAULT_CONFIG['img_mins']['size'],
            extensions=['mp3', 'jpg']
        )

        # Check _initialize_extension_map called
        fake_init_ext_map.assert_called_once_with(DEFAULT_CONFIG)

        # Check _list_directories called
        fake_listdir.assert_called_once_with()

        # Check _get_file_metadata called
        fake_get_metadata.assert_called_once_with(directories, analysis)

        # Check _calculate_percentages called
        fake_calculate.assert_called_once_with(analysis)

        # Check _export_to_json called with output_file
        with mock.patch('hd_recover.analyze._export_to_json') as fake_export:
            analyze.hdr_analyze(output_file='some file')
            fake_export.assert_called_once_with(analysis, 'some file',
                                                indent=2)

        # Check _export_to_json called with default config
        with mock.patch('hd_recover.analyze._export_to_json') as fake_export:
            analyze.hdr_analyze()
            fake_export.assert_called_once_with(analysis, 'hdr_analysis.json',
                                                indent=2)

        # Check _export_to_json not called when no_export is set
        with mock.patch('hd_recover.analyze._export_to_json') as fake_export:
            analyze.hdr_analyze(no_export=True)
            assert not fake_export.called

    # --- END PUBLIC METHOD --- #


if __name__ == '__main__':
    unittest.main()
