import json
import unittest
from unittest import mock
from datetime import datetime, timezone
from hd_recover import hdr_utils
from hd_recover.configuration import DEFAULT_CONFIG


class UtilsTestCase(unittest.TestCase):

    # ----- BEGIN TEST IMPORT METHODS ----- #

    def test__is_valid_import(self):
        # Check correct return for:
        #   empty dict
        analysis = {}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        #   analysis not dict
        analysis = 'not a dict'
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        #   missing top-level keys
        analysis = {'extensions': {}, 'dates': {}, 'ignored': {}}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        #   empty top-level dicts
        analysis = {
            'extensions': {'jpg': {'count': 0, 'size': 0, 'percent': 0.0}},
            'dates': {}, 'ignored': {}, 'config': DEFAULT_CONFIG,
            'total_size': 0}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        analysis = {
            'extensions': {},
            'dates': {'1975-06-10': {'count': 0, 'size': 0, 'percent': 0.0}},
            'ignored': {}, 'config': DEFAULT_CONFIG, 'total_size': 0}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        analysis = {
            'extensions': {'jpg': {'count': 0, 'size': 0, 'percent': 0.0}},
            'dates': {'1975-06-10': {'count': 0, 'size': 0, 'percent': 0.0}},
            'ignored': {}, 'config': {}, 'total_size': 0}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        #   incorrect types for top-level keys
        analysis = {
            'extensions': {}, 'dates': {}, 'ignored': {}, 'total_size': 'test'}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        analysis = {
            'extensions': {}, 'dates': {}, 'ignored': 1, 'total_size': 0}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        analysis = {
            'extensions': {}, 'dates': 1, 'ignored': {}, 'total_size': 0}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        analysis = {
            'extensions': 1, 'dates': {}, 'ignored': {}, 'total_size': 0}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        analysis = {
            'extensions': {0: {'count': 0, 'size': 0, 'percent': 0.0}},
            'dates': {'1975-06-10': {'count': 0, 'size': 0, 'percent': 0.0}},
            'ignored': {},
            'total_size': 0}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        analysis = {
            'extensions': {0: {'count': 0, 'size': 0, 'percent': 0.0}},
            'dates': {'1975-06-10': {'count': 0, 'size': 0, 'percent': 0.0}},
            'ignored': {},
            'total_size': 0}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        #   missing fields for extensions, dates, ignored files
        analysis = {
            'extensions': {'jpg': {}},
            'dates': {'1975-06-10': {'count': 0, 'size': 0, 'percent': 0.0}},
            'ignored': {'count': 0, 'size': 0, 'percent': 0.0},
            'total_size': 0}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        analysis = {
            'extensions': {'jpg': {'count': 0, 'size': 0, 'percent': 0.0}},
            'dates': {'1975-06-10': {}},
            'ignored': {'count': 0, 'size': 0, 'percent': 0.0},
            'total_size': 0}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        analysis = {
            'extensions': {'jpg': {'count': 0, 'size': 0, 'percent': 0.0}},
            'dates': {'1975-06-10': {'count': 0, 'size': 0, 'percent': 0.0}},
            'ignored': {},
            'total_size': 0}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        #   incorrect types for fields in extensions, dates, ignored files
        analysis = {
            'extensions': {'jpg': {'count': 'a', 'size': 0, 'percent': 0.0}},
            'dates': {'1975-06-10': {'count': 0, 'size': 0, 'percent': 0.0}},
            'ignored': {'count': 0, 'size': 0, 'percent': 0.0},
            'total_size': 0}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        analysis = {
            'extensions': {'jpg': {'count': 0, 'size': 'a', 'percent': 0.0}},
            'dates': {'1975-06-10': {'count': 0, 'size': 0, 'percent': 0.0}},
            'ignored': {'count': 0, 'size': 0, 'percent': 0.0},
            'total_size': 0}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        analysis = {
            'extensions': {'jpg': {'count': 0, 'size': 0, 'percent': 'a'}},
            'dates': {'1975-06-10': {'count': 0, 'size': 0, 'percent': 0.0}},
            'ignored': {'count': 0, 'size': 0, 'percent': 0.0},
            'total_size': 0}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        analysis = {
            'extensions': {'jpg': {'count': 0, 'size': 0, 'percent': 0.0}},
            'dates': {'1975-06-10': {'count': 'a', 'size': 0, 'percent': 0.0}},
            'ignored': {'count': 0, 'size': 0, 'percent': 0.0},
            'total_size': 0}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        analysis = {
            'extensions': {'jpg': {'count': 0, 'size': 0, 'percent': 0.0}},
            'dates': {'1975-06-10': {'count': 0, 'size': 'a', 'percent': 0.0}},
            'ignored': {'count': 0, 'size': 0, 'percent': 0.0},
            'total_size': 0}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        analysis = {
            'extensions': {'jpg': {'count': 0, 'size': 0, 'percent': 0.0}},
            'dates': {'1975-06-10': {'count': 0, 'size': 0, 'percent': 'a'}},
            'ignored': {'count': 0, 'size': 0, 'percent': 0.0},
            'total_size': 0}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        analysis = {
            'extensions': {'jpg': {'count': 0, 'size': 0, 'percent': 0.0}},
            'dates': {'1975-06-10': {'count': 0, 'size': 0, 'percent': 0.0}},
            'ignored': {'count': 'a', 'size': 0, 'percent': 0.0},
            'total_size': 0}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        analysis = {
            'extensions': {'jpg': {'count': 0, 'size': 0, 'percent': 0.0}},
            'dates': {'1975-06-10': {'count': 0, 'size': 0, 'percent': 0.0}},
            'ignored': {'count': 0, 'size': 'a', 'percent': 0.0},
            'total_size': 0}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        analysis = {
            'extensions': {'jpg': {'count': 0, 'size': 0, 'percent': 0.0}},
            'dates': {'1975-06-10': {'count': 0, 'size': 0, 'percent': 0.0}},
            'ignored': {'count': 0, 'size': 0, 'percent': 'a'},
            'total_size': 0}
        self.assertFalse(hdr_utils._is_valid_import(analysis))

        #   correctly formatted analysis
        analysis = {
            'extensions': {'jpg': {'count': 0, 'size': 0, 'percent': 0.0}},
            'dates': {'1975-06-10': {'count': 0, 'size': 0, 'percent': 0.0}},
            'ignored': {'count': 0, 'size': 0, 'percent': 0.0},
            'config': DEFAULT_CONFIG,
            'total_size': 0}
        self.assertTrue(hdr_utils._is_valid_import(analysis))

    def test_import_from_json(self):
        # Check open json.load called with json_file for valid JSON
        with mock.patch('hd_recover.hdr_utils.open') as fake_open:
            with mock.patch('hd_recover.hdr_utils.json') as fake_json:
                with mock.patch('hd_recover.hdr_utils._is_valid_import') \
                        as fake_valid:
                    fake_valid.return_value = True
                    fake_json.load.return_value = {}
                    hdr_utils.import_from_json('some file')

                    fake_json.load.assert_called_once_with(
                        fake_open.return_value.__enter__.return_value)
                    fake_valid.assert_called_once_with({})

        # Check program exit on UnicodeDecodeError
        json_exception = json.decoder.JSONDecodeError
        with mock.patch('hd_recover.hdr_utils.open') as fake_open:
            with mock.patch('hd_recover.hdr_utils.json') as fake_json:
                with mock.patch('hd_recover.hdr_utils.sys') as fake_sys:
                    fake_json.load.side_effect = UnicodeDecodeError(
                        '', b'', 0, 0, '')
                    fake_json.decoder.JSONDecodeError = json_exception
                    fake_sys.exit.side_effect = SystemExit
                    try:
                        hdr_utils.import_from_json('some file')
                    except SystemExit:
                        assert True
                    else:
                        assert False
                    fake_sys.exit.assert_called_once_with()

        # Check program exit on JSONDecodeError
        json_exception = json.decoder.JSONDecodeError
        with mock.patch('hd_recover.hdr_utils.open') as fake_open:
            with mock.patch('hd_recover.hdr_utils.json') as fake_json:
                with mock.patch('hd_recover.hdr_utils.sys') as fake_sys:
                    fake_json.load.side_effect = json_exception('', '', 0)
                    fake_json.decoder.JSONDecodeError = json_exception
                    fake_sys.exit.side_effect = SystemExit
                    try:
                        hdr_utils.import_from_json('some file')
                    except SystemExit:
                        assert True
                    else:
                        assert False
                    fake_sys.exit.assert_called_once_with()

        # Check program exit on FileNotFoundError
        with mock.patch('hd_recover.hdr_utils.open') as fake_open:
            with mock.patch('hd_recover.hdr_utils.json') as fake_json:
                with mock.patch('hd_recover.hdr_utils.sys') as fake_sys:
                    fake_open.side_effect = FileNotFoundError
                    fake_sys.exit.side_effect = SystemExit
                    try:
                        hdr_utils.import_from_json('some file')
                    except SystemExit:
                        assert True
                    else:
                        assert False
                    fake_sys.exit.assert_called_once_with()

    # ----- END TEST IMPORT METHODS ----- #

    # ----- BEGIN TEST UTILITY METHODS ----- #

    @mock.patch('hd_recover.hdr_utils.os')
    def test_sizeof(self, mock_os):
        # Check that stat is called with valid file path
        hdr_utils.sizeof('some file')
        mock_os.stat.assert_called_once_with('some file')

        # Check that None is returned on exception (FileNotFoundError)
        mock_os.stat.side_effect = FileNotFoundError
        self.assertEqual(0, hdr_utils.sizeof('some file'))

    @mock.patch('hd_recover.hdr_utils.os')
    def test_dateof(self, mock_os):
        # Check returns properly formatted date given:

        # - Valid time stamps
        mock_os.stat.return_value = mock_os.stat_result

        mock_os.stat_result.st_mtime = 0
        result = hdr_utils.dateof('some file')
        mock_os.stat.assert_called_once_with('some file')
        self.assertRegex(result, r'[1-2]\d{3}-[0-1]\d-[0-3]\d')

        mock_os.stat_result.st_mtime = 1500000000
        result = hdr_utils.dateof('some file')
        self.assertRegex(result, r'[1-2]\d{3}-[0-1]\d-[0-3]\d')

        # - Negative timestamp
        mock_os.stat_result.st_mtime = -1
        result = hdr_utils.dateof('some file')
        self.assertEqual(result, '1969-12-31')

        # - Future timestamp
        today = datetime.today().replace(tzinfo=timezone.utc).timestamp()
        mock_os.stat_result.st_mtime = today + 1
        result = hdr_utils.dateof('some file')
        self.assertEqual(result, '1969-12-31')

        # Check returns 0 on FileNotFoundError
        mock_os.stat.side_effect = FileNotFoundError
        self.assertEqual(hdr_utils.dateof('some file'), '1969-12-31')

    def test_strip_extension(self):
        # Check returns correct extension for:

        # - Normal extension
        result = hdr_utils.strip_extension('tester.mp3')
        self.assertEqual(result, 'mp3')

        # - Capitalized extension
        result = hdr_utils.strip_extension('tester.JPG')
        self.assertEqual(result, 'jpg')

        # - No extension
        result = hdr_utils.strip_extension('tester')
        self.assertEqual(result, 'unknown')

        # - Multiple '.'s in filename
        result = hdr_utils.strip_extension('tester.more.txt')
        self.assertEqual(result, 'txt')

    def test_format_size(self):
        # Check return valid size format for:
        #   < 1000 bytes
        self.assertEqual('500.00B', hdr_utils.format_size(500))

        #   < 1000 kilobytes
        self.assertEqual('1.50K', hdr_utils.format_size(1500))

        #   < 1000 megabytes
        self.assertEqual('20.00M', hdr_utils.format_size(20000000))

        #   < 1000 gigabytes
        self.assertEqual('699.00G', hdr_utils.format_size(699000000000))

        #   < 1000 terabytes
        self.assertEqual('47.30T', hdr_utils.format_size(47300000000000))

    def test_has_id3(self):
        # Check return valid for:
        #   no id3 tag
        with mock.patch('hsaudiotag.mpeg.Mpeg') as mocked_mpeg:
            mocked_mpeg.return_value = mock.Mock()
            mocked_mpeg.return_value.tag = None
            self.assertFalse(hdr_utils.has_id3('some file'))

        #   valid id3 tag
        with mock.patch('hsaudiotag.mpeg.Mpeg') as mocked_mpeg:
            mocked_mpeg.return_value = mock.Mock()
            mocked_mpeg.return_value.tag = not None
            self.assertTrue(hdr_utils.has_id3('some file'))

        # Check exception raised on FileNotFoundError:
        with mock.patch('hsaudiotag.mpeg.Mpeg') as mocked_mpeg:
            mocked_mpeg.side_effect = FileNotFoundError
            try:
                hdr_utils.has_id3('some file')
            except FileNotFoundError:
                assert True
            else:
                assert False

    def test_is_thumbnail(self):
        config = DEFAULT_CONFIG
        config['ignore_thumbnails'] = True
        config['img_mins']['size'] = 5000
        config['img_mins']['pixels'] = 100

        # Check return valid for:
        #   image size below minimum
        with mock.patch('hd_recover.hdr_utils.Image') as mocked_Image:
            with mock.patch('hd_recover.hdr_utils.sizeof', return_value=0):
                self.assertTrue(hdr_utils.is_thumbnail('some file', config))

        #   image dimensions below minimum
        with mock.patch('hd_recover.hdr_utils.Image') as mocked_Image:
            with mock.patch('hd_recover.hdr_utils.sizeof',
                            return_value=10000):
                mocked_Image.open.return_value = mock.Mock()
                mocked_Image.open.return_value.size = (50, 50)
                self.assertTrue(hdr_utils.is_thumbnail('some file', config))

        #   image size and dimensions above minimum
        with mock.patch('hd_recover.hdr_utils.Image') as mocked_Image:
            with mock.patch('hd_recover.hdr_utils.sizeof', return_value=10000):
                mocked_Image.open.return_value = mock.Mock()
                mocked_Image.open.return_value.size = (500, 500)
                self.assertFalse(hdr_utils.is_thumbnail('some file', config))

        #   OSError raised
        with mock.patch('hd_recover.hdr_utils.Image') as mocked_Image:
            with mock.patch('hd_recover.hdr_utils.sizeof', return_value=10000):
                mocked_Image.open.side_effect = OSError
                try:
                    hdr_utils.is_thumbnail('some file', config)
                except OSError:
                    assert True
                else:
                    assert False

    def test_is_ignored(self):
        config = DEFAULT_CONFIG
        config['min_size'] = 5000
        config['ignore_blanks'] = True
        config['ignore_thumbnails'] = True
        config['img_mins'] = {'pixels': 100, 'size': 5000}

        # Check return valid for:
        #   file size <= 0
        with mock.patch('hd_recover.hdr_utils.sizeof', return_value=-1):
            self.assertTrue(hdr_utils.is_ignored('some/file.jpg', config))

        #   file size < min_size
        with mock.patch('hd_recover.hdr_utils.sizeof', return_value=400):
            self.assertTrue(hdr_utils.is_ignored('some/file.jpg', config))

        #   mp3s without id3 tags
        with mock.patch('hd_recover.hdr_utils.has_id3', return_value=False):
            self.assertTrue(hdr_utils.is_ignored('some/file.mp3', config))

        #   ignored image formats
        with mock.patch('hd_recover.hdr_utils.is_thumbnail',
                        return_value=True):
            config['img_formats'] = ['bmp', 'jpg', 'gif']
            self.assertTrue(hdr_utils.is_ignored('some/file.gif', config))

        #   extension not configuration
        config['extensions'] = {'mp3': True, 'doc': True, 'gif': True}
        self.assertTrue(hdr_utils.is_ignored('some/file.cpp', config))

        #   none of the above, i.e. size > min_size, not mp3, not in
        #   img_formats, and in extensions configuration
        with mock.patch('hd_recover.hdr_utils.sizeof', return_value=40000):
            config['extensions'] = {'mp3': True, 'doc': True, 'gif': True}
            config['img_formats'] = ['bmp', 'jpg', 'gif']
            self.assertFalse(hdr_utils.is_ignored('some/file.doc', config))

    # --- END TEST UTILITY METHODS --- #


if __name__ == '__main__':
    unittest.main()
